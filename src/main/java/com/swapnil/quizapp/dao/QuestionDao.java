package com.swapnil.quizapp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.swapnil.quizapp.model.Question;

@Repository
public interface QuestionDao extends JpaRepository<Question, Integer>{
	List<Question> findByCategory(String category);
	//as we dont have direct method for findByCatogory , we have taken a middle path to get it.
	// category is part of table Question, so JPA of Spring boot is smart enough to recognize this method.
	
	@Query(value = "SELECT * FROM question q WHERE q.category=:category ORDER BY rand() LIMIT :numQ", nativeQuery = true )
	List<Question> findRandomQuestionsByCategory(String category, int numQ);
	

}
