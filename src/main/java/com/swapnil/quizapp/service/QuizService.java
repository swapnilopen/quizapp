package com.swapnil.quizapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swapnil.quizapp.dao.QuestionDao;
import com.swapnil.quizapp.dao.QuizDao;
import com.swapnil.quizapp.dto.QuestionDTO;
import com.swapnil.quizapp.model.Question;
import com.swapnil.quizapp.model.Quiz;
import com.swapnil.quizapp.model.Response;

@Service
public class QuizService {
	
	@Autowired
	QuizDao quizDao;
	
	@Autowired
	QuestionDao questionDao;
	
	@Autowired
	Response response;
	
	public String createQuiz(String category, int numQ, String title) {
		List<Question> questions = questionDao.findRandomQuestionsByCategory(category, numQ);
		for(Question que : questions) {
			System.out.println(que);
		}
		Quiz quiz = new Quiz();
		quiz.setTitle(title);
		quiz.setQuestions(questions);
		quizDao.save(quiz);
		return "success";
	}

	public List<QuestionDTO> getQuizQuestion(Integer quizNo) {
		Optional<Quiz> quiz = quizDao.findById(quizNo);
		List<Question> questionFromDB = quiz.get().getQuestions();
		List<QuestionDTO> questionForUser = new ArrayList<>();
		for(Question q : questionFromDB) {
			QuestionDTO qD = new QuestionDTO(q.getId(),q.getQuestionTitle(),q.getOption1(),q.getOption2(),q.getOption3(),q.getOption4()); 
			questionForUser.add(qD);
		}
		
		return questionForUser;
	}

	public String calculateResult(Integer id, List<Response> response) {
		Quiz quiz = quizDao.findById(id).get();
		List<Question> questions = quiz.getQuestions();
		int right =0;
		int i =0;
		for(Response r: response) {
			if(r.getResponse().equals(questions.get(i).getRightAnswer()))
				right++;
		}
		i++;
		return "success";
	}
}
