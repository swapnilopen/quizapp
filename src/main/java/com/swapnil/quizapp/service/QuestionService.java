package com.swapnil.quizapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.swapnil.quizapp.dao.QuestionDao;
import com.swapnil.quizapp.model.Question;

@Service
public class QuestionService {
	
	@Autowired
	QuestionDao questionDao;

	public List<Question> getAllQuestions() {
		// TODO Auto-generated method stub
		return questionDao.findAll();
	}

	public List<Question> getQuestionsByCategory(String category) {
		// TODO Auto-generated method stub
		return questionDao.findByCategory(category);
	}

	public String addQuestion(Question que) {
		questionDao.save(que);
		return "success";
	}

	public String deleteQuestion(int id) {
		questionDao.deleteById(id);
		return "Successfully deleted";
	}

	
}
