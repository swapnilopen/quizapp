package com.swapnil.quizapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.swapnil.quizapp.model.Question;
import com.swapnil.quizapp.service.QuestionService;

@RestController
@RequestMapping("/question")
public class QuestionController {

	@Autowired
	QuestionService questionService;

	@GetMapping("/allQuestions")
	public ResponseEntity<List<Question>> getAllQuestions() {
		try {
			return new ResponseEntity<>(questionService.getAllQuestions(), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
	}

	@GetMapping("/category/{cat}") // alternative for cat is category
	public ResponseEntity<List<Question>> getQuestionsByCategory(@PathVariable("cat") String category) { 
		//if we use category word for @getmapping, we dont need ("cat")										
		try {
			return new ResponseEntity<>(questionService.getQuestionsByCategory(category), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>(new ArrayList<>(), HttpStatus.BAD_REQUEST);
	}

	@PostMapping("/add")
	public ResponseEntity<String> addQuestion(@RequestBody Question que) {
		try {
			return new ResponseEntity<>(questionService.addQuestion(que),HttpStatus.CREATED);

		} catch (Exception e) {
			e.printStackTrace();

		}
		return new ResponseEntity<>("failed to add", HttpStatus.BAD_REQUEST);
		
	}

	@DeleteMapping("/delete/{id}") // through url we can directly send id of a question to be deleted
	public ResponseEntity<String> deleteQuestion(@PathVariable("id") int id) {
		try {
			return new ResponseEntity<>(questionService.deleteQuestion(id),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<>("failed to delete", HttpStatus.BAD_REQUEST);
	}
}
