package com.swapnil.quizapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.swapnil.quizapp.dto.QuestionDTO;
import com.swapnil.quizapp.model.Response;
import com.swapnil.quizapp.service.QuizService;

@RestController
@RequestMapping("/quiz")
public class QuizController {
	
	@Autowired
	QuizService quizService;
	
	@PostMapping("/create")
	public ResponseEntity<String> creatQuiz(@RequestParam String category, @RequestParam int numQ, @RequestParam String title){
		return new ResponseEntity<>(quizService.createQuiz(category, numQ, title) ,HttpStatus.OK);
		
	}
	@GetMapping("/get/{quizNo}")
	public ResponseEntity<List<QuestionDTO>> getQuizQuestion(@PathVariable Integer quizNo){
		
		return new ResponseEntity<>(quizService.getQuizQuestion(quizNo), HttpStatus.OK);
	}
	
	@PostMapping("/submit/{id}")
	public ResponseEntity<String> submitQuiz(@PathVariable Integer id, @RequestBody List<Response> response){
		
		return new ResponseEntity<>(quizService.calculateResult(id, response),HttpStatus.OK);
	}

}
